<?php

use App\Modules\Employee\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'first_name' => 'Bob',
            'last_name' => 'Kinto',
            'salary' => '1000000',
            'joining_date' => '2019-01-20',
            'department' => 'Kinto'
        ]);

        Employee::create([
            'first_name' => 'Jerry',
            'last_name' => 'Kansxo',
            'salary' => '6000000',
            'joining_date' => '2019-01-15',
            'department' => 'IT',
        ]);

        Employee::create([
            'first_name' => 'Philip',
            'last_name' => 'Jose',
            'salary' => '8900000',
            'joining_date' => '2019-02-05',
            'department' => 'Banking',
        ]);

        Employee::create([
            'first_name' => 'John',
            'last_name' => 'Abraham',
            'salary' => '2000000',
            'joining_date' => '2019-02-25',
            'department' => 'Insurance',
        ]);

        Employee::create([
            'first_name' => 'Michael',
            'last_name' => 'Mathew',
            'salary' => '2200000',
            'joining_date' => '2019-02-28',
            'department' => 'Finance',
        ]);

        Employee::create([
            'first_name' => 'Alex',
            'last_name' => 'chreketo',
            'salary' => '4000000',
            'joining_date' => '2019-05-10',
            'department' => 'IT',
        ]);

        Employee::create([
            'first_name' => 'Yohan',
            'last_name' => 'Soso',
            'salary' => '1230000',
            'joining_date' => '2019-06-20',
            'department' => 'Banking',
        ]);
    }
}

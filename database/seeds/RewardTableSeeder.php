<?php

use App\Modules\Reward\Models\Reward;
use Illuminate\Database\Seeder;

class RewardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reward::create([
            'employee_ref_id' => 1,
            'date_reward' => ' 2019-05-11',
            'amount' => '1000'
        ]);

        Reward::create([
            'employee_ref_id' => 2,
            'date_reward' => '2019-02-15',
            'amount' => '5000'
        ]);

        Reward::create([
            'employee_ref_id' => 3,
            'date_reward' => '2019-04-22',
            'amount' => '2000'
        ]);

        Reward::create([
            'employee_ref_id' => 1,
            'date_reward' => ' 2019-06-20',
            'amount' => '8000'
        ]);

    }
}

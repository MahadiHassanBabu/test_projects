<?php

Route::group(['module' => 'Employee', 'middleware' => ['web','auth'], 'namespace' => 'App\Modules\Employee\Controllers'], function() {

    Route::resource('employees', 'EmployeeController');

});

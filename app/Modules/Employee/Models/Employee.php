<?php

namespace App\Modules\Employee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model {

    protected $table = 'employees';
    protected $fillable = [
        'employee_id',
        'first_name',
        'last_name',
        'salary',
        'department',
        'joining_date',
        'created_at',
        'updated_at'
    ];

    public static function getEmployeeList()
    {
        return Employee::leftJoin('rewards','rewards.employee_ref_id','=','employees.employee_id')
            ->where('employees.last_name','LIKE',"%N")
            ->where('rewards.amount','<', '5100')
            ->where('employees.joining_date', '<=', date('Y-m-d', strtotime('2019-02-25')))
            ->orderBy('employees.joining_date','asc');
    }


}

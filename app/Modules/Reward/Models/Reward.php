<?php

namespace App\Modules\Reward\Models;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model {

    protected $table = 'rewards';
    protected $fillable = [
        'employee_ref_id',
        'date_reward',
        'amount',
        'created_at',
        'updated_at'
    ];

}

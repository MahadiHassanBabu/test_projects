<?php

Route::group(['module' => 'Reward', 'middleware' => ['web','auth'], 'namespace' => 'App\Modules\Reward\Controllers'], function() {

    Route::resource('rewards', 'RewardController');

});

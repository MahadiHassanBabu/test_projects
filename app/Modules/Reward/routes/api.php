<?php

Route::group(['module' => 'Reward', 'middleware' => ['api'], 'namespace' => 'App\Modules\Reward\Controllers'], function() {

    Route::resource('Reward', 'RewardController');

});

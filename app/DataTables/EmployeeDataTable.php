<?php

namespace App\DataTables;

use App\Modules\Employee\Models\Employee;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class EmployeeDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->rawColumns([])
            ->make(true);

    }

    /**
     * Get query source of dataTable.
     * @return \Illuminate\Database\Eloquent\Builder
     * @internal param \App\Models\AgentBalanceTransactionHistory $model
     */
    public function query()
    {
        $query = Employee::getEmployeeList();
        $data = $query->select([
            'employees.*',
            'rewards.date_reward',
            'rewards.amount'
        ]);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'dom'         => 'Blfrtip',
                'responsive'  => true,
                'autoWidth'   => false,
                'paging'      => true,
                "pagingType"  => "full_numbers",
                'searching'   => true,
                'info'        => true,
                'searchDelay' => 350,
                "serverSide"  => true,
                'order'       => [[1, 'asc']],
                'buttons'     => ['excel','csv', 'print', 'reset', 'reload'],
                'pageLength'  => 10,
                'lengthMenu'  => [[10, 20, 25, 50, 100, 500, -1], [10, 20, 25, 50, 100, 500, 'All']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'joining date'    => ['data' => 'joining_date', 'name' => 'employees.joining_date', 'orderable' => true, 'searchable' => true],
            'first name'       => ['data' => 'first_name', 'name' => 'employees.first_name', 'orderable' => true, 'searchable' => false],
            'last name'       => ['data' => 'last_name', 'name' => 'employees.last_name', 'orderable' => true, 'searchable' => false],
            'salary'          => ['data' => 'salary', 'name' => 'employees.salary', 'orderable' => true, 'searchable' => true],
            'department'      => ['data' => 'department', 'name' => 'employees.department', 'orderable' => true, 'searchable' => true],
            'date reward'     => ['data' => 'date_reward', 'name' => 'rewards.date_reward', 'orderable' => true, 'searchable' => true],
            'amount'          => ['data' => 'amount', 'name' => 'rewards.amount', 'orderable' => true, 'searchable' => true],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employee_list_' . date('Y_m_d_H_i_s');
    }
}
